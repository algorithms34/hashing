/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/tableSizeaven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.kongphop.hashing;

/**
 *
 * @author Admin
 */
public class Hashing<Key, Value> {

    private int M = 13;
    private Value[] vals = (Value[]) new Object[M];
    private Key[] keys = (Key[]) new Object[M];

    private int hash(Key key) {
        return (key.hashCode() & 0X7FFFFFFF) % M;
    }

    public Value get(Key key) {
        int i = hash(key);
        while (keys[i] != null) {
            if (key.equals(keys[i])) {
                return vals[i];
            }
            i = (i + 1) % M;
        }
        return null;
    }

    public void put(Key key, Value val) {
        int i = hash(key);
        while (keys[i] != null) {
            if (key.equals(keys[i])) {
                keys[i] = key;
                vals[i] = val;
            }
            i = (i + 1) % M;
        }
        keys[i] = key;
        vals[i] = val;
    }

    public int remove(Key key) {
        int i = hash(key);
        while (keys[i] != null) {
            if (key.equals(keys[i])) {
                int temp = (int) keys[i];
                keys[i] = null;
                return temp;
            }
            i = (i + 1) % M;
        }
        return 0;
    }

}
