/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kongphop.hashing;

import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class TestHashMap {

    public static void main(String[] args) {
        Hashing<Integer, String> h = new Hashing<Integer, String>();

        h.put(101, "Red");
        h.put(102, "Yellow");
        h.put(103, "Green");
    
        System.out.println(h.get(103));
        System.out.println(h.get(102));
        System.out.println(h.get(101));
        
        h.remove(102);
        System.out.println("");
        
        System.out.println(h.get(103));
        System.out.println(h.get(102));
        System.out.println(h.get(101));
    }

}
